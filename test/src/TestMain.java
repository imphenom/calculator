import com.company.Main;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMain {
    public Main main;

    @Test
    public void testAdd(){
        main = new Main();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first parameter:");
        int a = sc.nextInt();
        System.out.println("Enter second parameter");
        int b = sc.nextInt();
        System.out.println("Enter expected result");
        int expected = sc.nextInt();
        assertEquals(expected, main.add(a, b));
    }
    @Test
    public void testSubtract(){
        main = new Main();
		Scanner sc = new Scanner(System.in);
        System.out.println("Enter first parameter:");
        int a = sc.nextInt();
        System.out.println("Enter second parameter");
        int b = sc.nextInt();
        System.out.println("Enter expected result");
        int expected = sc.nextInt();
        assertEquals(expected, main.subtract(a, b));
    }
    @Test
    public void testMultiply(){
        main = new Main();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter first parameter:");
        int a = sc.nextInt();
        System.out.println("Enter second parameter");
        int b = sc.nextInt();
        System.out.println("Enter expected result");
        int expected = sc.nextInt();
        assertEquals(expected, main.multiply(a, b));
    }
    @Test
    public void testDivide(){
        main = new Main();
		Scanner sc = new Scanner(System.in);
        System.out.println("Enter first parameter:");
        int a = sc.nextInt();
        System.out.println("Enter second parameter");
        int b = sc.nextInt();
        System.out.println("Enter expected result");
        int expected = sc.nextInt();
        assertEquals(expected, main.divide(a, b));
    }
}
